class ReleasesController {
    constructor(API) {
        'ngInject';

        this.API = API;
    }

    $onInit() {
        this.API.all('releases').get('').then((response) =>{
            this.releases = response;
        })
        console.log(this.API);
    }
}
export const ReleasesComponent = {
    templateUrl: './views/app/components/releases/releases.component.html',
    controller: ReleasesController,
    controllerAs: 'vm',
    bindings: {}
}
