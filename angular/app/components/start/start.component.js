class StartController{
    constructor(){
        'ngInject';

        //
    }

    $onInit(){
    }
}

export const StartComponent = {
    templateUrl: './views/app/components/start/start.component.html',
    controller: StartController,
    controllerAs: 'vm',
    bindings: {}
}
