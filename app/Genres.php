<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genres extends Model
{
    public function videogenres(){

        return $this->belongsTo(VideoGenres::class);
    }
}
