<?php

namespace App\Http\Controllers;

use App\Video;

use Illuminate\Http\Request;

use App\Http\Requests;

class AllVideoController extends Controller
{
    public function all()
    {
        $videos = Video::type()->with(['videogenres'=> function($query){
            $query->with('genres');
        } ])->get();


        return $videos;
    }
}
