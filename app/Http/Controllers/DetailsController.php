<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;

use App\Http\Requests;

class DetailsController extends Controller
{
    public function show($type,$videoName){
        return Video::byAlias($videoName);
    }
}
