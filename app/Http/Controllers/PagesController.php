<?php

namespace App\Http\Controllers;


class PagesController extends Controller
{
    public function home()
    {
    	return view('welcome');
    }
     public function about()
    {
    	return view('pages.about');
    }
    public function test()
    {

        $t = \App\Video::where('type',4)->with('videoepisodes')->with('videoreleases')->get();

        return $t;

    }
     public function contact()
    {
        return view('pages.contact');

    }

}
