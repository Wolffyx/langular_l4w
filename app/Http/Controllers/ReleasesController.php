<?php

namespace App\Http\Controllers;

use App\Video;
use App\VideoEpisodes;

use Illuminate\Http\Request;

use App\Http\Requests;

class ReleasesController extends Controller
{
    public function releases()
    {
       return  response()->success(VideoEpisodes::releases());
    }

}
