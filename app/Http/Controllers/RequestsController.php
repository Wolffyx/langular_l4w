<?php

namespace App\Http\Controllers;

use App\VideoRequests;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\VideoRequest;

class RequestsController extends Controller
{

    public function requestanime()
    {
        $animerequest = VideoRequests::where('video_id_type',1)->get();

        return $animerequest;
//        return view('pages.requests');
    }

    /**
     * @param VideoRequest $request
     * @return mixed
     */
    public function addrequestanime(VideoRequest $request)
    {

//        VideoRequests::create([
//            'request_name ' => $request->request_name,
//            'video_name'    => $request->video_name,
//            'reason'        => $request->reason,
//            'video_id_type' => '1',
//        ]);
        $anime = new VideoRequests;

        $anime->request_name = $request->request_name;
        $anime->video_name = $request->video_name;
        $anime->reason = $request->reason;
        $anime->video_id_type = 1;
        $anime->status = 'Waiting';
        $anime->save();
        return $request;
    }
}
