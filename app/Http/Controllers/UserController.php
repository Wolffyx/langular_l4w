<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Htauthtp\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        return User::where('name', $user)->
        with(['watching' => function ($query) {
            $query->count();
        }])->
        with(['plantowatch' => function ($query) {
            $query->count();
        }])->
        with(['favourite' => function ($query) {
            $query->count();
        }])->
        with(['complete' => function ($query) {
            $query->count();
        }])->
        get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        $userId = User::where('name', $user)->pluck('id')->first();
        if (Auth::login($user, true)) {
            return $userId;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function favourite($user)
    {
        return User::where('name', $user)->with('favourite')->get();
    }
    public function complete($user)
    {
        return User::where('name', $user)->with('complete')->get();
    }
    public function plantowatch($user)
    {
        return User::where('name', $user)->with('plantowatch')->get();
    }
    public function watching($user)
    {
        return User::where('name', $user)->with('watching')->get();
    }

    public function test(Request $request)
    {
        return $request->user();
    }
}
