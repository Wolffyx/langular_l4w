<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Releases extends Model
{
    public function videoepisodes(){

        return $this->belongsTo(VideoEpisodes::class);
    }

}
