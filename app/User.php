<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function watching()
    {
        return $this->hasMany(WatchingList::class);
    }
    public function plantowatch()
    {
        return $this->hasMany(PlanList::class);
    }
    public function favourite()
    {
        return $this->hasMany(Favourite::class);
    }
    public function complete()
    {
        return $this->hasMany(Complete::class);
    }
}
