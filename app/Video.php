<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class Video extends Model
{
    public function scopeType()
    {
        $type = VideoTypes::where('type', Request::segment(2))->pluck('id')->first();
        return $this->where('type', $type);
    }

    public function videogenres()
    {

        return $this->hasMany(VideoGenres::class);
    }

    public function genres()
    {

        return $this->hasOne(Genres::class);
    }

    public function videoreleases()
    {

        return $this->hasMany(VideoEpisodes::class);
    }

    public function videolinks()
    {

        return $this->hasMany(VideoLinks::class);
    }

    public function scopeByAlias($builder, $alias)
    {

        return $this->type()->where('video_alias', $alias)
            ->with(['videogenres.genres'/*=> function($query){$query->with('genres');}*/])
            ->with('videoreleases')
            ->first();
    }

    public function scopeWatching()
    {
        $videoHash = $this->type()->
        where('video_alias', $this->Name)->
        with(['videoreleases' => function ($query) {
            $query->with(['video_episode', 'video_hash']);
        }])->
        get();
        return $videoHash;
    }

}

