<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoEpisodes extends Model
{
    /**
     * @return mixed
     */
    public function scopeReleases()
    {
        $releases = Video::type()->with('videoreleases')->get();

        return $releases;
    }

    public function videolinks()
    {
        return $this->hasMany(VideoLinks::class,'video_episode_id');
    }
    public function videoName()
    {
        return $this->belongsTo(Video::class,'video_id');
    }
}