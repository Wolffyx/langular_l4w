<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoGenres extends Model
{
    public function genres(){

        return $this->hasOne(Genres::class,'id','genre_id');
    }
}
