<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoLinks extends Model
{
    public function language(){

        return $this->hasOne(Languages::class,'id','language_id');
    }
    public function videoEpisode(){

        return $this->belongsTo(VideoEpisodes::class);
    }
}
