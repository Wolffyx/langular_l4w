<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVideoEpisodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('video_episodes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('video_id')->nullable();
			$table->integer('video_episode')->nullable();
			$table->char('video_hash', 10)->unique();
			$table->integer('views')->nullable();
			$table->dateTime('release_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('video_episodes');
	}

}
