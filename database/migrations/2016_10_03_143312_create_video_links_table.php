<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVideoLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('video_links', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('video_id')->nullable();
			$table->integer('video_episode_id');
			$table->text('link', 65535)->nullable();
			$table->integer('language_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('video_links');
	}

}
