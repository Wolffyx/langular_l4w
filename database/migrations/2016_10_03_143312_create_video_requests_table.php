<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVideoRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('video_requests', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('request_name', 60)->nullable();
			$table->string('video_name', 60)->nullable();
			$table->integer('video_id_type');
			$table->text('reason', 65535)->nullable();
			$table->dateTime('created_at')->nullable();
			$table->string('status', 25)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('video_requests');
	}

}
