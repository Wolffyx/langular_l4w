<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToImgsPatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('imgs_pats', function(Blueprint $table)
		{
			$table->foreign('user_id', 'USER_IMG_ID')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('imgs_pats', function(Blueprint $table)
		{
			$table->dropForeign('USER_IMG_ID');
			$table->dropForeign('imgs_pats_user_id_foreign');
		});
	}

}
