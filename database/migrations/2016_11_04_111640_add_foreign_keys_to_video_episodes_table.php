<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVideoEpisodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('video_episodes', function(Blueprint $table)
		{
			$table->foreign('video_id', 'video_episodes_videos_id_fk')->references('id')->on('videos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('video_episodes', function(Blueprint $table)
		{
			$table->dropForeign('video_episodes_videos_id_fk');
		});
	}

}
