<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVideoGenresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('video_genres', function(Blueprint $table)
		{
			$table->foreign('genre_id', 'video_genres_genres_id_fk')->references('id')->on('genres')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('video_id', 'video_genres_videos_id_fk')->references('id')->on('videos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('video_genres', function(Blueprint $table)
		{
			$table->dropForeign('video_genres_genres_id_fk');
			$table->dropForeign('video_genres_videos_id_fk');
		});
	}

}
