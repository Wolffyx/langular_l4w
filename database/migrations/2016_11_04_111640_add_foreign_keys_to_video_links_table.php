<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVideoLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('video_links', function(Blueprint $table)
		{
			$table->foreign('language_id', 'links_languages_id_fk')->references('id')->on('languages')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('video_id', 'links_videos_id_fk')->references('id')->on('videos')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('video_episode_id', 'video_links_video_episodes_id_fk')->references('id')->on('video_episodes')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('video_links', function(Blueprint $table)
		{
			$table->dropForeign('links_languages_id_fk');
			$table->dropForeign('links_videos_id_fk');
			$table->dropForeign('video_links_video_episodes_id_fk');
		});
	}

}
