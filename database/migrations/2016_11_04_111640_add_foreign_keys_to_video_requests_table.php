<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToVideoRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('video_requests', function(Blueprint $table)
		{
			$table->foreign('video_id_type', 'requests_video_type_id_fk')->references('id')->on('video_type')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('video_requests', function(Blueprint $table)
		{
			$table->dropForeign('requests_video_type_id_fk');
		});
	}

}
