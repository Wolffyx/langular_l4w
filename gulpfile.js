'use strict';

const elixir = require('laravel-elixir');

require('laravel-elixir-eslint');

require('./tasks/swPrecache.task.js');
require('./tasks/bower.task.js');

// setting assets paths
elixir.config.assetsPath = './';
elixir.config.css.folder = 'angular';
elixir.config.css.sass.folder = 'angular';
elixir.config.js.folder = 'angular';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 let assets = [
         'public/js/final.js',
         'public/css/final.css'
     ],
     scripts = [
         // 'bower_components/angular/angular.min.js',
         // 'bower_components/angular-animate/angular-animate.min.js',
         // 'bower_components/angular-aria/angular-aria.min.js',
         // 'bower_components/angular-bootstrap/ui-bootstrap.min.js',
         // 'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
         // 'bower_components/angular-material/angular-material.min.js'',,
         // 'bower_components/angular-material/angular-material-mocks.js
         // 'bower_components/angular-messages/angular-messages.min.js',
         // 'bower_components/angular-ui-router/release/angular-ui-router.min.js',
         'public/js/vendor.js',
         'public/js/app.js'
     ],
     styles = [
         // for some reason, ./ prefix here works fine!
         // it is needed to override elixir.config.css.folder for styles mixin
         './public/css/vendor.css',
         './public/css/app.css'
     ],
     karmaJsDir = [
         'public/js/vendor.js',
         'node_modules/angular-mocks/angular-mocks.js',
         'node_modules/ng-describe/dist/ng-describe.js',
         'public/js/app.js',
         'tests/angular/**/*.spec.js'
     ];

elixir(mix => {
    mix.bower()
       .copy('angular/app/**/*.html', 'public/views/app/')
       .webpack('index.main.js', 'public/js/app.js')
       .sass(['**/*.scss', 'critical.scss'], 'public/css')
       .sass('critical.scss', 'public/css/critical.css')
       .styles(styles, 'public/css/final.css')
       .eslint('angular/**/*.js')
       .combine(scripts, 'public/js/final.js')
       // .version(assets)
       .swPrecache();


       //enable front-end tests by adding the below task
       // .karma({jsDir: karmaJsDir});
});