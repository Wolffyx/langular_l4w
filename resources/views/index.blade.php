<!doctype html>
<html ng-app="app" ng-strict-di>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Live4Watching</title>

    <meta name="theme-color" content="#444444">

    <link rel="manifest" href="manifest.json">

    <!--[if lte IE 10]>
    <script type="text/javascript">document.location.href = '/unsupported-browser'</script>
    <![endif]-->
    <style><?php require(public_path("css/critical.css")) ?></style>
    {{--<script src="{!! elixir('js/final.js') !!}" async></script>--}}
    {{--<link rel="stylesheet" href="{!! elixir('css/final.css') !!}">--}}
    <script src="js/final.js" async></script>
    <link rel="stylesheet" href="css/final.css">
</head>
<body>
    <app-root></app-root>
</body>
</html>
