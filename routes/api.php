<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    Route::post('auth/login', 'Auth\AuthController@login');
    Route::post('auth/register', 'Auth\AuthController@register');

    Route::post('auth/password/email', 'Auth\PasswordResetController@sendResetLinkEmail');
    Route::get('auth/password/verify', 'Auth\PasswordResetController@verify');
    Route::post('auth/password/reset', 'Auth\PasswordResetController@reset');

    Route::get('user/{user}/test', 'UserController@test');

    Route::get('user/{user}/favourite', 'UserController@favourite');
    Route::get('user/{user}/watching-plan', 'UserController@plantowatch');
    Route::get('user/{user}/watching', 'UserController@watching');
    Route::get('user/{user}/complete', 'UserController@complete');
    Route::resource('user', 'UserController', ['only' => ['show','update','edit']]);

    Route::get('{type}', 'ReleasesController@releases');
    Route::get('{type}/all', 'AllVideoController@all');
    Route::get('{type}/new', 'VideosController@new');
    Route::get('{type}/popular', 'VideosController@popular');
    Route::get('{type}/requests', 'Controller@requests');
    Route::get('{type}/watching={videoHash}', 'WatchingController@watch');
    Route::get('{type}/{videoName}', 'DetailsController@show');

//protected API routes with JWT (must be logged in)
Route::get('/user', 'UserController@test')->middleware('auth:api');

